<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => Podbase\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'podio' => [
        'client_id'             => env('PODIO_CLIENT_ID'),
        'client_secret'         => env('PODIO_CLIENT_SECRET'),
        'contacts_app_id'       => env('PODIO_CONTACTS_APP_ID'),
        'contacts_app_token'    => env('PODIO_CONTACTS_APP_TOKEN'),
        'deals_app_id'          => env('PODIO_DEALS_APP_ID'),
        'deals_app_token'       => env('PODIO_DEALS_APP_TOKEN'),
        'assignees_app_id'      => env('PODIO_ASSIGNEES_APP_ID'),
        'assignees_app_token'   => env('PODIO_ASSIGNEES_APP_TOKEN'),

    ],

    'basecrm' => [
        'client_pat'        => env('BASECRM_PAT'),
        'client_id'         => env('BASECRM_CLIENT_ID'),
        'client_secret'     => env('BASECRM_CLIENT_SECRET'),
        'username'          => env('BASECRM_USERNAME'),
        'password'          => env('BASECRM_PASSWORD'),
    ],

];

<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\PodioAuthController as PodioAuth;
use Podbase\Http\Controllers\BaseCRMNotesController as BaseNotes;
use Podbase\Http\Controllers\BaseCRMDealsController as BaseDeals;
use Podbase\Http\Controllers\BaseCRMContactsController as BaseContacts;
use PodioHook;
use PodioItem;
use PodioComment;


class PodioHooksController extends Controller
{
	private $setup;
	private $deals_app_id;
	private $deals_app_token;
	private $contacts_app_id;
	private $contacts_app_token;

    public function __construct(PodioAuth $auth)
	{
		$this->deals_app_id = config()->get('services')['podio']['deals_app_id'];
		$this->deals_app_token = config()->get('services')['podio']['deals_app_token'];
		$this->contacts_app_id = config()->get('services')['podio']['contacts_app_id'];
		$this->contacts_app_token = config()->get('services')['podio']['contacts_app_token'];
		$this->setup = $auth;
	}

	public function registerHooks()
	{	// create app hook for deals
		$this->setup->auth_for_app($this->deals_app_id, $this->deals_app_token);
		// comment hook
		$this->createAppHook('app', $this->deals_app_id, [
			'url'	=> url('/podio/hooks/comments').'?app_id='.$this->deals_app_id,
			'type'	=> 'comment.create',
		]);

		// update hook
		$this->createAppHook('app_field', 126298077, [
			'url'	=> url('/podio/hooks/deals/update').'?app_id='.$this->deals_app_id,
			'type'	=> 'item.update',
		]);

		//create app hook for contacts
		$this->setup->auth_for_app($this->contacts_app_id, $this->contacts_app_token);
		// comment hook
		$this->createAppHook('app', $this->contacts_app_id, [
			'url'	=> url('/podio/hooks/comments').'?app_id='.$this->contacts_app_id,
			'type'	=> 'comment.create',
		]);

        // update hook
        $this->createAppHook('app_field', 126298170, [
            'url'   => url('/podio/hooks/contacts/update').'?app_id='.$this->contacts_app_id,
            'type'  => 'item.update',
        ]);
	}

	private function createAppHook($ref_type, $ref_id, $attributes = array())
	{
		$hook = PodioHook::create($ref_type, $ref_id, $attributes);

		return $hook;
	}

    public function getContactsHooks()
    {
        $this->setup->auth_for_app($this->contacts_app_id, $this->contacts_app_token);
        $hooks = PodioHook::get_for('app_field', 126298170);
        $deleted = array();
        foreach($hooks as $hook)
        {
            $deleted[] = PodioHook::delete($hook->hook_id);
        }

        return dd($deleted);
    }

    public function getDealsHooks()
    {
        $this->setup->auth_for_app($this->deals_app_id, $this->deals_app_token);
        $hooks = PodioHook::get_for('app_field', 126298077);
        $deleted = array();
        foreach($hooks as $hook)
        {
            $deleted[] = PodioHook::delete($hook->hook_id);
        }

        return dd($deleted);
    }

    public function commentHook(Request $request, BaseNotes $base_notes, BaseDeals $base_deals)
    {
    	switch($request['type']){
    		case 'hook.verify':
    			$validate = PodioHook::validate($request['hook_id'], ['code' => $request['code']]);
    		break;
    		case 'comment.create':
    			$resource_type = '';

    			if($request['app_id'] == $this->deals_app_id) {
    				$this->setup->auth_for_app($this->deals_app_id, $this->deals_app_token);
    				$resource_type = 'deal';
    			}
    			elseif($request['app_id'] == $this->contacts_app_id) {
    				$this->setup->auth_for_app($this->contacts_app_id, $this->contacts_app_token);
    				$resource_type = 'contact';
    			}

    			$item = PodioItem::get($request['item_id']);
				$base_crm_id = $item->fields['base-crm-id']->values;
				$comment = PodioComment::get($request['comment_id']);

                //check if comment begins with FOU
                //if so, we dont push it to BaseCRM
                if(strpos($comment->value, 'FOU') === false){
    				$note = $comment->created_by->name . ' said via Podbase: '. $comment->value;

    				$arr = [
    					'content'			=> $note,
    					'resource_type'		=> $resource_type,
    					'resource_id'		=> (int)$base_crm_id,
    				];

    				$new_note = $base_notes;
    				return $new_note->createNote($arr);
                }
    		break;
    	}
    }

    public function dealsUpdateHook(Request $request, BaseDeals $base_deals)
    {
    	switch($request['type']){
    		case 'hook.verify':
    			$validate = PodioHook::validate($request['hook_id'], ['code' => $request['code']]);
    		break;
    		case 'item.update':
    		$this->setup->auth_for_app($this->deals_app_id, $this->deals_app_token);
    			// 100% - Closed Won : 59938

    			$deal_item = PodioItem::get($request['item_id']);

    			$action_trigger_id = isset($deal_item->fields['action-triggers']->values) ? $deal_item->fields['action-triggers']->values[0]['id'] : null;

    			if($action_trigger_id == 2){

                    $base_crm_id = $deal_item->fields['base-crm-id']->values;

    				$arr = [ 'stage_id'	    => 59938,
    		                 'value'		=> isset($deal_item->fields['value']->values) ? $deal_item->fields['value']->values : 0,
                    ];

    				$update_deal = $base_deals;
    				return $update_deal->updateDeal((int)$base_crm_id, $arr);
    			}
                elseif($action_trigger_id == 1){

                    $required_flag = false;
                    $error_flag = "";
                    if(!isset($deal_item->fields['contact']->values)){
                        $required_flag = true;
                        $error_flag .= "FOU **System Message:** Contact reference is required! \n";
                    }

                    if(!isset($deal_item->fields['assignee']->values))
                    {
                        $required_flag = true;
                        $error_flag .= "FOU **System Message:** Assignee reference is required! \n";
                    }

                    if($required_flag == false){

                        $contact_item_id = $deal_item->fields['contact']->values[0];
                        $contact_item = PodioItem::get($contact_item_id->item_id);

                        $assignee_item_id = $deal_item->fields['assignee']->values[0];
                        $assignee_item = PodioItem::get($assignee_item_id->item_id);

                        if(isset($contact_item->fields['base-crm-id']->values)){
                            // 5% - Received RFQ = 59937
                            $arr = [
                                        'name'                  => $deal_item->fields['title']->values,
                                        'contact_id'            => (integer) $contact_item->fields['base-crm-id']->values,
                                        'owner_id'              => (integer) $assignee_item->fields['base-crm-id']->values,
                                        'value'                 => isset($deal_item->fields['value']->values) ? $deal_item->fields['value']->values : 0,
                                        'currency'              => isset($deal_item->fields['currency']->values) ? $deal_item->fields['currency']->values : 'USD',
                                        'estimated_close_date'  => isset($deal_item->fields['estimated-close-date']->start) ? date('Y-m-d',strtotime($deal_item->fields['estimated-close-date']->start)) : null,
                                        'custom_fields'         => ['PodioSource1'  => 'Yes'],
                                    ];

                            $create_deal = $base_deals;
                            $base_deal = $create_deal->createDeal($arr);

                            $update_item = ($base_deal) ? PodioItem::update($request['item_id'], array(
                                'fields' => array('base-crm-id' => (string)$base_deal['id'],)
                            )) : null;

                        }
                        else{
                            $value = "FOU **System Message:** Contact must be pushed to Base CRM first!";

                            $comment = PodioComment::create('item', $request['item_id'],
                                                           ['value' => $value,
                                                            'hook' => false]);
                        }

                    }
                    else{

                        $comment = PodioComment::create('item', $request['item_id'],
                                                       ['value' => $error_flag,
                                                        'hook' => false]);

                        return $comment;
                    }

                }
    		break;
    	}
    }

    public function contactsUpdateHook(Request $request, BaseContacts $base_contacts)
    {
    	switch($request['type']){
    		case 'hook.verify':
    			$validate = PodioHook::validate($request['hook_id'], ['code' => $request['code']]);
    		break;
    		case 'item.update':

                $this->setup->auth_for_app($this->contacts_app_id, $this->contacts_app_token);
                $contact_item = PodioItem::get($request['item_id']);
                $action_trigger_id = isset($contact_item->fields['action-triggers']->values) ? $contact_item->fields['action-triggers']->values[0]['id'] : null;

                if($action_trigger_id == 1){

                    $arr = [
                        'is_organization'       => $contact_item->fields['organization']->values == "" ? false : true,
                        'name'                  => isset($contact_item->fields['organization']->values) ? $contact_item->fields['organization']->values : '',
                        'first_name'            => isset($contact_item->fields['first-name']->values) ? $contact_item->fields['first-name']->values : '',
                        'last_name'             => isset($contact_item->fields['last-name']->values) ? $contact_item->fields['last-name']->values : '',
                        'email'                 => isset($contact_item->fields['email']->values) ? $contact_item->fields['email']->values[0]['value'] : '',
                        'phone'                 => isset($contact_item->fields['phone']->values) ? $contact_item->fields['phone']->values[0]['value'] : '',
                        'twitter'               => isset($contact_item->fields['twitter']->values) ? $contact_item->fields['twitter']->values[0]->original_url : null,
                        'facebook'              => isset($contact_item->fields['facebook']->values) ? $contact_item->fields['facebook']->values[0]->original_url : null,
                        'linkedin'              => isset($contact_item->fields['linkedin']->values) ? $contact_item->fields['linkedin']->values[0]->original_url : null,
                        'website'               => isset($contact_item->fields['website']->values) ? $contact_item->fields['website']->values[0]->original_url : null,
                        'description'           => isset($contact_item->fields['description']->values) ? $contact_item->fields['description']->values : '',
                        'custom_fields'         => ['PodioSource1'  => 'Yes'],
                    ];

                    $create_contacts = $base_contacts;
                    $base_contact = $create_contacts->createContact($arr);

                    $update_item = ($base_contact) ? PodioItem::update($request['item_id'], array(
                        'fields' => array('base-crm-id' => (string)$base_contact['id'],)
                    )) : null;
                }

    		break;
    	}
    }
}

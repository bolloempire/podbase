<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\BaseCRMAuthController as BaseAuth;

class BaseCRMNotesController extends Controller
{
    private $client;

	public function __construct(BaseAuth $auth)
	{
		$this->client = $auth->setup();
	}

	public function getNotesByResourceId($resource_id = 0)
	{
		if($resource_id == null) return null;

    	$notes = $this->client->notes->all(['resource_id' => $resource_id]);

    	return ($notes) ? $notes : null;
	}

	public function createNote($array = [])
	{
		if($array == null || empty($array)) return null;

		$note = $this->client->notes->create($array);

		return $note;
	}
}

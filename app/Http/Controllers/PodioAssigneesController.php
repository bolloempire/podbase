<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\PodioAuthController as PodioAuth;
use PodioItem;
use PodioError;
use PodioEmbed;
use PodioSearchResult;

class PodioAssigneesController extends Controller
{
    private $setup;
	private $app_id;
	private $app_token;

	public function __construct(PodioAuth $auth)
	{
		$this->app_id = config()->get('services')['podio']['assignees_app_id'];
		$this->app_token = config()->get('services')['podio']['assignees_app_token'];
		$this->setup = $auth;
	}

	public function findAssignee($id)
	{
		$this->setup->auth_for_app($this->app_id, $this->app_token);
		$result = PodioSearchResult::app($this->app_id, ['query' => (string) $id, 'limit' => 1]);
		if($result){
			return $result[0]->id;
		}

		return null;
	}

}

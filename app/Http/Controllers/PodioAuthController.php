<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request as Request;
use Podbase\Http\Controllers\PodioSessionManagerController as PodioSession;
use Podio;


class PodioAuthController extends Controller
{
    private $client_id;
    private $client_secret;

    public function __construct()
    {
    	$this->client_id = config()->get('services')['podio']['client_id'];
        $this->client_secret = config()->get('services')['podio']['client_secret'];
        $this->setup();
    }


    public function setup()
    {
    	Podio::setup($this->client_id, $this->client_secret, array('session_manager' => 'PodioSession'));
    }


    public function auth_for_app($app_id, $app_token)
    {
        Podio::$auth_type = array(
            'type' => 'app',
            'identifier' => $app_id,
        );

        Podio::$oauth = PodioSession::get(Podio::$auth_type);

        if(!Podio::is_authenticated())
        {
            Podio::authenticate_with_app($app_id, $app_token);
        }

    }


}

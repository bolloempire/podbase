<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\PodioAuthController as PodioAuth;
use PodioItem;
use PodioError;
use PodioEmbed;
use PodioSearchResult;

class PodioContactsController extends Controller
{

	private $setup;
	private $app_id;
	private $app_token;

	public function __construct(PodioAuth $auth)
	{
		$this->app_id = config()->get('services')['podio']['contacts_app_id'];
		$this->app_token = config()->get('services')['podio']['contacts_app_token'];
		$this->setup = $auth;
	}

	public function createContact($data)
	{
		if($data){
			$this->setup->auth_for_app($this->app_id, $this->app_token);
			try{
				$item = PodioItem::create($this->app_id, ['fields' => $this->setFields($data),]);
			}
			catch(PodioError $e){
				return null;
			}
			return $item;
		}
		return null;
	}

	public function findContact($id)
	{
		$this->setup->auth_for_app($this->app_id, $this->app_token);
		$result = PodioSearchResult::app($this->app_id, ['query' => (string) $id, 'limit' => 1]);
		if($result){
			return $result[0]->id;
		}

		return null;
	}

	public function updateContact($item_id, $data)
	{
		if($data){
			$this->setup->auth_for_app($this->app_id, $this->app_token);
			try {
				$item = PodioItem::update($item_id, ['fields' => $this->setFields($data),]);
			}
			catch(PodioError $e){
				return null;
			}
			return $item;
		}
		return null;
	}

	public function setFields($array)
	{
		return $fields = [
			'organization'			=> ($this->setStringField($array['properties']['is_organization'])) ? $this->setStringField($array['properties']['name']) : null,
			'first-name'			=> $this->setStringField($array['properties']['first_name']),
			'last-name' 			=> $this->setStringField($array['properties']['last_name']),
			'title'				 	=> $this->setStringField($array['properties']['title']),
			'email'					=> $this->setEmailField($array['properties']['email']),
			'phone'					=> $this->setPhoneField($array['properties']['phone']),
			'twitter'				=> $this->setLinkField($array['properties']['twitter']),
			'facebook'				=> $this->setLinkField($array['properties']['facebook']),
			'linkedin'				=> $this->setLinkField($array['properties']['linkedin']),
			'website'				=> $this->setLinkField($array['properties']['website']),
			'description'			=> $this->setStringField($array['properties']['description']),
			'industry'				=> $this->setStringField($array['properties']['industry']),
			'base-crm-id'			=> $this->setStringField((string) $array['properties']['id']),
		];
	}

	public function setStringField($string)
	{
		if($string == null || $string == '') {
			return null;
		}
		return $string;
	}

	public function setEmailField($emails)
	{
		$array = [];

		if($emails == null) {
			return null;
		}
		$array[] = array('type' => 'work', 'value' => $emails);
		return $array;
	}

	public function setPhoneField($numbers)
	{
		$array = [];

		if($numbers == null || $numbers == '') {
			return null;
		}
		$array[] = array('type' => 'work', 'value' => $numbers);
		return $array;
	}

	public function setLinkField($links)
	{
		if($links == null) {
			return null;
		}

		$array[] = $links;

		return PodioEmbed::create($array)->embed_id;
	}

}

<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use BaseCRM\Sync as Sync;
use Podbase\Http\Controllers\BaseCRMAuthController as BaseAuth;
use Podbase\Http\Controllers\PodioAuthController as PodioAuth;

use Podbase\Http\Controllers\BaseCRMDealsController as BaseDeals;
use Podbase\Http\Controllers\BaseCRMContactsController as BaseContacts;
use Podbase\Http\Controllers\BaseCRMNotesController as BaseNotes;

use Podbase\Http\Controllers\PodioDealsController as PodioDeals;
use Podbase\Http\Controllers\PodioContactsController as PodioContacts;
use Podbase\Http\Controllers\PodioCommentsController as PodioComments;
use Podbase\Http\Controllers\PodioAssigneesController as PodioAssignees;


class BaseCRMSyncController extends Controller
{

	private $client;
	private $uuid;

	private $podio_contacts;
	private $podio_deals;
	private $podio_comments;
	private $podio_assignees;
	private $base_contacts;
	private $base_deals;
	private $base_notes;

	public $sync_data = array();


	public function __construct(BaseAuth $base_auth, PodioAuth $podio_auth)
	{
		$this->client = $base_auth->setup();
		$this->uuid = 'PODBASEUUID82';
		// Instantiate podio controllers dependencies
		$this->podio_contacts = new PodioContacts($podio_auth);
		$this->podio_deals = new PodioDeals($podio_auth);
		$this->podio_comments = new PodioComments($podio_auth);
		$this->podio_assignees = new PodioAssignees($podio_auth);
		// Instantiate baseCRM controllers dependencies
		$this->base_contacts = new BaseContacts($base_auth);
		$this->base_deals = new BaseDeals($base_auth);
		$this->base_notes = new BaseNotes($base_auth);
	}


	public function sync()
	{
		$sync = new Sync($this->client, $this->uuid);
		$sync_data = $sync->fetch(function($meta, $data)
		{
			$options = ['table' => $meta['type'],
						'statement' => $meta['sync']['event_type'],
						'properties' => $data,
			];
			return $this->execute($options) ? Sync::ACK : Sync::NACK;
		});
		return $this->funnel();
		// return dd($this->sync_data);
	}

	public function execute($options)
	{
		if(!empty($options)){

			if(($options['table'] == 'deal' || $options['table'] == 'contact' || $options['table'] == 'note') &&
				($options['statement'] == 'created' || $options['statement'] == 'updated')){
				$this->sync_data[] = $options;
				return true;
			}
			return false;
		}
		return false;
	}

	public function funnel()
	{
		$this->funnelContacts();
		$this->funnelDeals();
		$this->funnelNotes();
	}

	public function funnelContacts()
	{
		foreach($this->sync_data as $data){
			// loop through contacts first so we can reference it later to deals
			if($data['table'] == 'contact') {
				switch($data['statement']) {
					case 'created':
						if(!isset($data['properties']['custom_fields']['PodioSource1']) || $data['properties']['custom_fields']['PodioSource1'] != "Yes") {
							$contact = $this->createContact($data);
						}
					break;
					case 'updated':
						$contact_id = $this->podio_contacts->findContact($data['properties']['id']);
						if($contact_id) { $contact = $this->updateContact($contact_id, $data); }
					break;
				}
			}
		}
	}

	public function funnelDeals()
	{
		foreach($this->sync_data as $data) {
			if($data['table'] == 'deal') {
				switch($data['statement']) {
					case 'created':
						if(!isset($data['properties']['custom_fields']['PodioSource1']) || $data['properties']['custom_fields']['PodioSource1'] != "Yes") {
							$deal = $this->createDeal($data);
						}
					break;
					case 'updated':
						$deal_id = $this->podio_deals->findDeal($data['properties']['id']);
						if($deal_id) { $deal = $this->updateDeal($deal_id, $data); }
					break;
				}
			}
		}
	}

	public function funnelNotes()
	{
		foreach($this->sync_data as $data){
			if($data['table'] == 'note'){

				$item_id = 0;

				if(strpos($data['properties']['content'], 'via Podbase') === false){
					if($data['properties']['resource_type'] == 'deal'){
						$item_id = $this->podio_deals->findDeal($data['properties']['resource_id']);
					}
					elseif($data['properties']['resource_type'] == 'contact'){
						$item_id = $this->podio_contacts->findContact($data['properties']['resource_id']);
					}

					if($item_id != 0){
						$comment = $this->podio_comments->createComment('item', $item_id, $data['properties']['content']);
					}
				}
			}
		}
	}

	public function createContact($data)
	{
		return $this->podio_contacts->createContact($data);
	}

	public function updateContact($item_id, $data)
	{
		return $this->podio_contacts->updateContact($item_id, $data);
	}

	public function createDeal($data)
	{
		// Replace stage_id and loss_reason_id with names from BaseCRM
		$data['properties']['stage_id'] = $this->base_deals->getStageName($data['properties']['stage_id']);
		$data['properties']['loss_reason_id'] = $this->base_deals->getLossReasonName($data['properties']['loss_reason_id']);

		//Find related contact from contacts app
		$data['properties']['contact_id'] = $this->podio_contacts->findContact($data['properties']['contact_id']);

		//Find assignee for this deal
		$data['properties']['owner_id'] = $this->podio_assignees->findAssignee($data['properties']['owner_id']);

		//Push to create a deal in Podio
		return $this->podio_deals->createDeal($data);
	}

	public function updateDeal($item_id, $data)
	{
		// Replace stage_id and loss_reason_id with names from BaseCRM
		$data['properties']['stage_id'] = $this->base_deals->getStageName($data['properties']['stage_id']);
		$data['properties']['loss_reason_id'] = $this->base_deals->getLossReasonName($data['properties']['loss_reason_id']);
		//Find related contact from contacts app
		$data['properties']['contact_id'] = $this->podio_contacts->findContact($data['properties']['contact_id']);

		//Push to create a deal in Podio
		return $this->podio_deals->updateDeal($item_id, $data);
	}

}

<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\BaseCRMAuthController as BaseAuth;

class BaseCRMDealsController extends Controller
{

	private $client;

	public function __construct(BaseAuth $auth)
	{
		$this->client = $auth->setup();
	}


    public function getStageById($id = 0)
    {
    	if($id == null) return null;

    	$stage = $this->client->stages->all(['ids' => $id]);

    	return $stage;
    }


    public function getStageName($id = 0)
    {
    	if($id == null) return null;

    	$name =  $this->getStageById($id);

    	return $name[0]['data']['name'];
    }


    public function getLossReasonById($id = null)
    {
    	if($id == null) return null;

    	$loss_reason = $this->client->lossReasons->get($id);

    	return $loss_reason;
    }


    public function getLossReasonName($id = 0)
    {
    	if($id == null) return null;

    	$loss_reason = $this->getLossReasonById($id);

    	return $loss_reason['name'];
    }


    public function updateDeal($id, $attributes = [])
    {
        $deal = $this->client->deals->update($id, $attributes);

        return $deal;
    }

    public function createDeal($attributes = [])
    {
        return $this->client->deals->create($attributes);
    }

}

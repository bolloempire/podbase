<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\PodioAuthController as PodioAuth;
use PodioItem;
use PodioError;
use PodioComment;

class PodioCommentsController extends Controller
{

	public function createComment($type = 'item', $id, $value)
	{
		if($value){
			$comment = PodioComment::create($type, $id, ['value' => $value, 'hook' => false]);
			return $comment;
		}
		return null;
	}
}

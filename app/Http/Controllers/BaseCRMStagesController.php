<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\BaseCRMAuthController as BaseAuth;

class BaseCRMStagesController extends Controller
{
    private $client;

	public function __construct(BaseAuth $auth)
	{
		$this->client = $auth->setup();
	}

	public function getStages()
	{
		$stages = $this->client->stages->all();

		return dd($stages); 
	}
}

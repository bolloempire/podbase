<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\PodioAuthController as PodioAuth;
use PodioItem;
use PodioError;
use PodioSearchResult;

class PodioDealsController extends Controller
{

	private $setup;
	private $app_id;
	private $app_token;

    public function __construct(PodioAuth $auth)
	{
		$this->app_id = config()->get('services')['podio']['deals_app_id'];
		$this->app_token = config()->get('services')['podio']['deals_app_token'];
		$this->setup = $auth;
	}


	public function createDeal($data)
	{
		if($data){
			$this->setup->auth_for_app($this->app_id, $this->app_token);
			try{
				$item = PodioItem::create($this->app_id, ['fields' => $this->setFields($data),]);
			}
			catch(PodioError $e){
				return null;
			}
			return $item;
		}
		return null;
	}

	public function findDeal($id)
	{
		$this->setup->auth_for_app($this->app_id, $this->app_token);
		$result = PodioSearchResult::app($this->app_id, ['query' => (string) $id, 'limit' => 1]);
		if($result){
			return $result[0]->id;
		}

		return null;
	}

	public function updateDeal($item_id, $data)
	{
		if($data){
			$this->setup->auth_for_app($this->app_id, $this->app_token);
			try {
				$item = PodioItem::update($item_id, ['fields' => $this->setFields($data),]);
			}
			catch(PodioError $e){
				return null;
			}
			return $item;
		}
		return null;
	}


	public function setFields($array)
	{
		return $fields = [
			'title' 				=> $this->setStringField($array['properties']['name']),
			'contact'				=> $this->setStringField($array['properties']['contact_id']),
			'assignee'				=> $this->setStringField($array['properties']['owner_id']),
			'stage'					=> $this->setStringField($array['properties']['stage_id']),
			'date-created' 			=> $this->setDateField($array['properties']['created_at']),
			'estimated-close-date' 	=> $this->setDateField($array['properties']['estimated_close_date']),
			'value' 				=> $this->setStringField($array['properties']['value']),
			'currency' 				=> $array['properties']['currency'],
			'dropbox-email'			=> $this->setStringField($array['properties']['dropbox_email']),
			'loss-reason'			=> $this->setStringField($array['properties']['loss_reason_id']),
			'base-crm-id'			=> $this->setStringField((string) $array['properties']['id']),
		];
	}

	public function setStringField($string)
	{
		if($string == null || $string == '') {
			return null;
		}
		return $string;
	}

	public function setDateField($date)
    {
        if($date == null) return null;
        return date('Y-m-d H:i:s', strtotime($date));
    }

}

<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use Podbase\Http\Controllers\BaseCRMAuthController as BaseAuth;

class BaseCRMContactsController extends Controller
{

    private $client;

	public function __construct(BaseAuth $auth)
	{
		$this->client = $auth->setup();
	}

	public function createContact($attributes = [])
    {
        return $this->client->contacts->create($attributes);
    }
}

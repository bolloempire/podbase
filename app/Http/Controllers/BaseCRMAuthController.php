<?php

namespace Podbase\Http\Controllers;

use Illuminate\Http\Request;
use \BaseCRM\Client;
use \GuzzleHttp\Client as Guzzle;

class BaseCRMAuthController extends Controller
{
    private $client_pat;
    private $client_id;
    private $client_secret;
    private $username;
    private $password;
    private $baseauth_url;
    private $redirect_url;
    private $state;
    private $access_token;
    private $guzzle;


    public function __construct(Guzzle $guzzle)
    {
    	$this->client_pat = config()->get('services')['basecrm']['client_pat'];
    	$this->client_id = config()->get('services')['basecrm']['client_id'];
        $this->client_secret = config()->get('services')['basecrm']['client_secret'];
        $this->username = config()->get('services')['basecrm']['username'];
        $this->password = config()->get('services')['basecrm']['password'];
        $this->baseauth_url = 'https://api.getbase.com/oauth2/token';
        $this->redirect_url = url('/') . 'oauth/base';
        $this->state = session()->token();
        $this->guzzle = $guzzle;
    }


    public function setup()
    {
    	$request = $this->guzzle->request('POST', $this->baseauth_url,

    		['form_params' => ['client_id' => $this->client_id,
                        'client_secret' => $this->client_secret,
    					'grant_type' => 'password',
                        'username' => $this->username,
                        'password' => $this->password,
    				   ],
    	]);

        $body = json_decode($request->getBody(true)->getContents());

    	$this->access_token = $body->access_token;

        $client = new \BaseCRM\Client(['accessToken' => $this->access_token]);

        return $client;
    }

}
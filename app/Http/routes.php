
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/oauth/base', 'BaseCRMAuthController@setup');
Route::get('/base/sync', 'BaseCRMSyncController@sync');
Route::get('/base/stages', 'BaseCRMDealsController@getStageName');
Route::get('/base/loss_reasons', 'BaseCRMDealsController@getLossReasonById');
Route::get('/base/notes','BaseCRMNotesController@getNotesByResourceId');
Route::get('/base/stages','BaseCRMStagesController@getStages');


Route::get('/podio/contacts', 'PodioContactsController@test');
Route::get('/podio/contacts/search', 'PodioContactsController@findContact');
Route::get('/podio/hooks/register', 'PodioHooksController@registerHooks');
Route::post('/podio/hooks/comments', 'PodioHooksController@commentHook');
Route::post('/podio/hooks/deals/update', 'PodioHooksController@dealsUpdateHook');
Route::post('/podio/hooks/contacts/update', 'PodioHooksController@contactsUpdateHook');
Route::get('/podio/hooks/deals/remove', 'PodioHooksController@getDealsHooks');
Route::get('/podio/hooks/contacts/remove', 'PodioHooksController@getContactsHooks');
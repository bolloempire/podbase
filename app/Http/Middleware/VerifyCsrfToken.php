<?php

namespace Podbase\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/podio/hooks/comments',
        '/podio/hooks/deals/update',
        '/podio/hooks/contacts/update',
    ];
}
